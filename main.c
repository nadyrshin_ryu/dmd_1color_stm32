//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "main.h"
#include <delay.h>
#include <disp1color.h>
#include <dmd_1color.h>
#include <font.h>
#include <dmd_demo.h>


void main()
{
  SystemInit();
  
  disp1color_Init();
  disp1color_FillScreenbuff(0);
  disp1color_UpdateFromBuff();

  uint8_t ScreenNum = 0;

  while (1)
  {
    uint16_t Period;
    uint16_t TickNum = screen_start(ScreenNum, &Period);
    
    while (TickNum--)
    {    
      screen_tick(ScreenNum);
      delay_ms(Period);
    }
    
    if (++ScreenNum == SCREEN_NUM)
      ScreenNum = 0;
  }
}
