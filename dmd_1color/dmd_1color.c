//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <delay.h>
#include <timers.h>
#include <dmd_1color.h>
#include <disp1color.h>
#include <gpio.h>
#include <spim.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


#define DMD_1COLOR_A_HIGH()     GPIO_WriteBit(DMD_1COLOR_A_Port, DMD_1COLOR_A_Pin, Bit_SET) 
#define DMD_1COLOR_A_LOW()      GPIO_WriteBit(DMD_1COLOR_A_Port, DMD_1COLOR_A_Pin, Bit_RESET)
#define DMD_1COLOR_B_HIGH()     GPIO_WriteBit(DMD_1COLOR_B_Port, DMD_1COLOR_B_Pin, Bit_SET)
#define DMD_1COLOR_B_LOW()      GPIO_WriteBit(DMD_1COLOR_B_Port, DMD_1COLOR_B_Pin, Bit_RESET)
#define DMD_1COLOR_SCLK_HIGH()  GPIO_WriteBit(DMD_1COLOR_SCLK_Port, DMD_1COLOR_SCLK_Pin, Bit_SET)
#define DMD_1COLOR_SCLK_LOW()   GPIO_WriteBit(DMD_1COLOR_SCLK_Port, DMD_1COLOR_SCLK_Pin, Bit_RESET)
#define DMD_1COLOR_nOE_HIGH()   GPIO_WriteBit(DMD_1COLOR_nOE_Port, DMD_1COLOR_nOE_Pin, Bit_SET)
#define DMD_1COLOR_nOE_LOW()    GPIO_WriteBit(DMD_1COLOR_nOE_Port, DMD_1COLOR_nOE_Pin, Bit_RESET)


// ������� ������ � ��������
uint8_t DMD_1COLOR_ScreenHeight;
uint8_t DMD_1COLOR_ScreenWidth;

uint8_t DMD_1COLOR_MatrixBuff[(DMD_1COLOR_MatrixHeight >> 3) * DMD_1COLOR_MatrixWidth];
uint8_t DMD_1COLOR_ScreenPart = 0;
uint8_t DMD_1COLOR_Buff[4][(DISP1COLOR_Width >> 3) * (DISP1COLOR_Height >> 2)];


//==============================================================================
// ���������, ������� ���������� �� ������� � �������� ��������
//==============================================================================
void DMD_TimerISR(void)
{
  DMD_1COLOR_SendPartToMatrix();
}
//==============================================================================


//==============================================================================
// ���������, ������� ���������� �� ��������� �����-�������� �� SPI
//==============================================================================
void DMD_SPI_End(void)
{
  // ��������� ������� �� ����� ������������ ������ ����������� � ������������ ������ � ��������
  DMD_1COLOR_nOE_LOW();

  // �������� ������ (0..3) � ������� �������� A � B
  switch (DMD_1COLOR_ScreenPart)
  {
  case 0:
    DMD_1COLOR_A_LOW();
    DMD_1COLOR_B_LOW();
    break;
  case 1:
    DMD_1COLOR_A_HIGH();
    DMD_1COLOR_B_LOW();
    break;
  case 2:
    DMD_1COLOR_A_LOW();
    DMD_1COLOR_B_HIGH();
    break;
  case 3:
    DMD_1COLOR_A_HIGH();
    DMD_1COLOR_B_HIGH();
    break;
  }
  
  // ����������� ���������� ������ � ������� �������� �� ����� SCLK
  DMD_1COLOR_SCLK_HIGH();
  DMD_1COLOR_SCLK_LOW();
  // �������� �������
  DMD_1COLOR_nOE_HIGH();
}
//==============================================================================


//==============================================================================
// ������� ���������� ���� (������ �� 8 �����������) �� 8-�������� ��������� ������
// ����������� ���������� � ����� ��������������� ������ ����������� �� ���������
//==============================================================================
uint8_t DMD_1COLOR_GetFrom8ScreenBytes(uint8_t Mask, uint8_t *pBuff)
{
  uint8_t Byte = 0;
  
  for (uint8_t BitMask = 0x80; BitMask; BitMask >>= 1, pBuff++)
  {
    if (*pBuff & Mask)
      Byte |= BitMask;
  }
  
#if (DMD_1COLOR_Inverse == 1)
  Byte ^= 0xFF;
#endif

  return Byte;
}
//==============================================================================


//==============================================================================
// ��������� ��������� 1 �������
//==============================================================================
void DMD_1COLOR_SendFromMatrixBuff(uint8_t Row4, uint8_t *pBuff)
{
  uint8_t MatrixRows8bit = DMD_1COLOR_MatrixHeight >> 3;
  uint8_t MatrixCols8bit = DMD_1COLOR_MatrixWidth >> 3;
  uint8_t *pPart = (uint8_t *) &(DMD_1COLOR_Buff[Row4][0]);
  
  for (uint8_t Col = 0; Col < MatrixCols8bit; Col++)
  {
    for (uint8_t Row = MatrixRows8bit; Row; Row--)
    {
      uint8_t *p8Bytes = pBuff + ((Row - 1) * DMD_1COLOR_MatrixWidth);
      p8Bytes += (Col << 3);
      
      *(pPart++) = DMD_1COLOR_GetFrom8ScreenBytes(1 << (Row4 + 4), p8Bytes);
      *(pPart++) = DMD_1COLOR_GetFrom8ScreenBytes(1 << Row4, p8Bytes);
    }
  }
}
//==============================================================================


//==============================================================================
// ��������� ��������� ��������� ��� ������� ������
//==============================================================================
void DMD_1COLOR_UpdateRow4(uint8_t Row4, uint8_t *pBuff, uint16_t BuffLen)
{
  // �������������� ������ ��� ��������
  uint16_t MatrixInRow = DMD_1COLOR_ScreenWidth / DMD_1COLOR_MatrixWidth; 
  uint16_t MatrixInCol = DMD_1COLOR_ScreenHeight / DMD_1COLOR_MatrixHeight;
  uint8_t MatrixLines8 = DMD_1COLOR_MatrixHeight >> 3;
  uint16_t StartIdxRow = 0, StartIdxCol = 0;
  
  for (uint8_t Row = 0; Row < MatrixInRow; Row++)
  {
    StartIdxCol = StartIdxRow;
    
    for (uint8_t Col = 0; Col < MatrixInCol; Col++)
    {
      // �������� ����� ����� ����� �������
      uint8_t *pDst = DMD_1COLOR_MatrixBuff;
      uint8_t *pSrc = &pBuff[StartIdxCol];
      
      for (uint8_t Line8 = 0; Line8 < MatrixLines8; Line8++)
      {
        memcpy(pDst, pSrc, DMD_1COLOR_MatrixWidth);
        DMD_1COLOR_SendFromMatrixBuff(Row4, DMD_1COLOR_MatrixBuff);
        pDst += DMD_1COLOR_MatrixWidth;
        pSrc += DMD_1COLOR_ScreenWidth;
      }
     
      StartIdxCol += DMD_1COLOR_MatrixWidth;
    }    
    StartIdxRow += MatrixLines8 * DMD_1COLOR_ScreenWidth;
  }
}
//==============================================================================




//==============================================================================
// ��������� �������������� ����� �� �������� � ����� � ������������ � ������� pBuff ����������� �����������
//==============================================================================
void DMD_1COLOR_DisplayFullUpdate(uint8_t *pBuff, uint16_t BuffLen)
{
  DMD_1COLOR_UpdateRow4(0, pBuff, BuffLen);
  DMD_1COLOR_UpdateRow4(1, pBuff, BuffLen);
  DMD_1COLOR_UpdateRow4(2, pBuff, BuffLen);
  DMD_1COLOR_UpdateRow4(3, pBuff, BuffLen);
}
//==============================================================================


//==============================================================================
// ��������� ��������� �������� ������ (Row4 - �� 0 �� 3 - ����� ������ ��� ����������)
//==============================================================================
void DMD_1COLOR_SendToScreen(uint8_t Row4, void *pBuff, uint16_t BuffLen)
{
  DMD_1COLOR_ScreenPart = Row4;
  
  // ��������� ������ ����������� 
#if (DMD_1COLOR_SPI_mode == DMD_1COLOR_SPI_IRQ)
  SPI_send8b_irq(DMD_1COLOR_SPI_periph, pBuff, BuffLen, DMD_SPI_End);
#elif (DMD_1COLOR_SPI_mode == DMD_1COLOR_SPI_DMA)
  SPI_send8b_dma(DMD_1COLOR_SPI_periph, pBuff, BuffLen, DMD_SPI_End);
#elif (DMD_1COLOR_SPI_mode == DMD_1COLOR_SPI_Polling)
  SPI_send8b(DMD_1COLOR_SPI_periph, pBuff, BuffLen);
  // ���� �������� � SPI ���������, �� ��������� ���������� ����� ���
  DMD_SPI_End();
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������� � ������� 1/4 ����� ������ �����
//==============================================================================
void DMD_1COLOR_SendPartToMatrix(void)
{
  // ��������� � ��������� ������ �����������
  DMD_1COLOR_ScreenPart++;
  DMD_1COLOR_ScreenPart &= 0x3;
  // ��������� ��������� ������ �����������
  DMD_1COLOR_SendToScreen(DMD_1COLOR_ScreenPart, &(DMD_1COLOR_Buff[DMD_1COLOR_ScreenPart][0]), (DISP1COLOR_Width >> 2) * (DISP1COLOR_Height >> 3));
}
//==============================================================================


//==============================================================================
// ��������� ��������� ����� ���������������� ��� ������ � ��������
//==============================================================================
void DMD_1color_GPIO_init(void)
{
  // ������� ������������� ���. ������ �� ������
  DMD_1COLOR_A_LOW();
  DMD_1COLOR_B_LOW();
  DMD_1COLOR_SCLK_LOW();
  DMD_1COLOR_nOE_LOW();
  
  // ����������� ��� ������������ ���� ��� ������
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
  
  // ������ A (��� ������ ������ ��� ����������)
  gpio_PortClockStart(DMD_1COLOR_A_Port);
  GPIO_InitStruct.GPIO_Pin = DMD_1COLOR_A_Pin;
  GPIO_Init(DMD_1COLOR_A_Port, &GPIO_InitStruct);
  // ������ B (��� ������ ������ ��� ����������)
  gpio_PortClockStart(DMD_1COLOR_B_Port);
  GPIO_InitStruct.GPIO_Pin = DMD_1COLOR_B_Pin;
  GPIO_Init(DMD_1COLOR_B_Port, &GPIO_InitStruct);
  // ������ SCLK 
  gpio_PortClockStart(DMD_1COLOR_SCLK_Port);
  GPIO_InitStruct.GPIO_Pin = DMD_1COLOR_SCLK_Pin;
  GPIO_Init(DMD_1COLOR_SCLK_Port, &GPIO_InitStruct);
  // ������ nOE (���������� ������ �������)
  gpio_PortClockStart(DMD_1COLOR_nOE_Port);
  GPIO_InitStruct.GPIO_Pin = DMD_1COLOR_nOE_Pin;
  GPIO_Init(DMD_1COLOR_nOE_Port, &GPIO_InitStruct);
}
//==============================================================================


//==============================================================================
// ��������� ������������� �������
//==============================================================================
void DMD_1color_Init(uint8_t Width, uint8_t Height)
{
  DMD_1COLOR_ScreenWidth = Width;
  DMD_1COLOR_ScreenHeight = Height;
  
  // ������������� ����� ���������� ���������� ���������
  DMD_1color_GPIO_init();
  // ������������� ���������� SPI
  spim_init(DMD_1COLOR_SPI_periph, 8);
  // ������������� ������� ���������� ������
  tmr2_init(DMD_1COLOR_RefreshRate, DMD_TimerISR);
  tmr2_start();
}
//==============================================================================
