//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _DMD_1COLOR_H
#define _DMD_1COLOR_H

#include <types.h>


#define DMD_1COLOR_SPI_Polling          0       // ��� ����������, ��� � ����� ��������� �������� �����
#define DMD_1COLOR_SPI_IRQ              1       // �� ��������� �������� �����, ����������� ��������� � ����������� ���������� �� SPI
#define DMD_1COLOR_SPI_DMA              2       // ��������� DMA ������ � ������� ���� ����������, �� ��������� �������� �������� � ���������� ���������� DMA


// ������� 1 ������� � ������ ����������� ������ (����������� ��������� ���������� ������� � ��������)
#define DMD_1COLOR_MatrixHeight         16
#define DMD_1COLOR_MatrixWidth          32

// ������� (� ��) ���������� �������� ������ �� ������ DMD (����� ������� ���������� �� �������2)
#define DMD_1COLOR_RefreshRate          400
// �������� ������ ����� ��������� �� SPI (�� ���� ������� �����)
#define DMD_1COLOR_Inverse              1
// ������������ SPI (SPI1, SPI2)
#define DMD_1COLOR_SPI_periph           SPI1
// ����� ������ � SPI
#define DMD_1COLOR_SPI_mode             DMD_1COLOR_SPI_DMA


//==============================================================================
// ��������� ����������� � ������������� ������� �������
// ������ R ��������� �� ����� MOSI ���������������� (������������� ��� ������������� ����������� SPI ������)
// ������ CLK ��������� �� ����� SCK ���������������� (������������� ��� ������������� ����������� SPI ������)
//==============================================================================
// ������ A - ��� ������ ������ ����������� (1..4)
#define DMD_1COLOR_A_Port       GPIOB
#define DMD_1COLOR_A_Pin        GPIO_Pin_0
// ������ B - ��� ������ ������ ����������� (1..4)
#define DMD_1COLOR_B_Port       GPIOB
#define DMD_1COLOR_B_Pin        GPIO_Pin_1
// ������ SCLK (����� �������� ������, ����������� �� �������� ������� �� ���� �����)
#define DMD_1COLOR_SCLK_Port    GPIOB
#define DMD_1COLOR_SCLK_Pin     GPIO_Pin_10
// ������ nOE (���������� ������ �������)
#define DMD_1COLOR_nOE_Port     GPIOB
#define DMD_1COLOR_nOE_Pin      GPIO_Pin_11
//==============================================================================


// ��������� ������������� ������ � ��������� DMD
void DMD_1color_Init(uint8_t Width, uint8_t Height);
// ��������� ������� � ������� ����� ����� �� ������� pBuff
void DMD_1COLOR_DisplayFullUpdate(uint8_t *pBuff, uint16_t BuffLen);
// ��������� ������� � ������� 1/4 ����� ������ �����
void DMD_1COLOR_SendPartToMatrix(void);
// ��������� ��������� �������� ������ (Row4 - �� 0 �� 3 - ����� ������ ��� ����������)
void DMD_1COLOR_SendToScreen(uint8_t Row4, void *pBuff, uint16_t BuffLen);

#endif
